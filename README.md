# Description
Dataset on Amazon's Top 50 bestselling books from 2009 to 2019. Contains 550 books, data has been categorized into fiction and non-fiction using Goodreads

## Note
The dataset was taken from kaggle public dataset.
https://www.kaggle.com/sootersaalu/amazon-top-50-bestselling-books-2009-2019
